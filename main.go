// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"github.com/streadway/amqp"
	"log"
	"net/http"
)

var addr = flag.String("addr", ":8082", "http service address")

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	wordPtr := flag.String("server", "amqp://guest:guest@localhost:5672/", "a string")
	flag.Parse()

	conn, err := amqp.Dial(*wordPtr)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"broadcast", // name
		true,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	hub := newHub()
	go hub.run()

	go func() {
		for d := range msgs {
			hub.broadcast <- d.Body;
			log.Printf("Received a message: %s", d.Body)
		}
	}()

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	err_listen := http.ListenAndServe(*addr, nil)
	if err_listen != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}